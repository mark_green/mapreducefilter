package mapreducefilter

import (
	"reflect"
)

// Usage: mapreduce.FilterGeneric(slice, filter).([]ResultType)
func FilterGeneric(input interface{}, filter interface{}) interface{} {
    vInput := reflect.ValueOf(input)
    vFilter := reflect.ValueOf(filter)

    tOutput := reflect.SliceOf(vInput.Type())
    vOutput := reflect.MakeSlice(tOutput, 0, vInput.Len())

    for i := 0; i < vInput.Len(); i++ {
    	vItem := vInput.Index(i)
        include := vFilter.Call([]reflect.Value{vItem})[0]
        if include.Interface().(bool) {
        	vOutput.Set(reflect.Append(vOutput, vItem))
        }
    }
    return vOutput.Interface()
}

func FilterString(input []string, filter func(string) bool) []string {
	output := make([]string, 0, len(input))
	for _, v := range input {
		if filter(v) {
			output = append(output, v)
		}
	}
	return output
}
