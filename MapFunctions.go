package mapreducefilter

import (
	"reflect"
)

// Usage: mapreduce.MapGeneric(slice, transform).([]ResultType)
func MapGeneric(input interface{}, transform interface{}) interface{} {
    vInput := reflect.ValueOf(input)
    vTransform := reflect.ValueOf(transform)

    tOutput := reflect.SliceOf(vTransform.Type().Out(0))
    vOutput := reflect.MakeSlice(tOutput, vInput.Len(), vInput.Len())

    for i := 0; i < vInput.Len(); i++ {
        transformed := vTransform.Call([]reflect.Value{vInput.Index(i)})[0]
        vOutput.Index(i).Set(transformed)
    }
    return vOutput.Interface()
}

func MapString(input []string, transform func(string) string) []string {
	output := make([]string, len(input))
	for i, v := range input {
        output[i] = transform(v)
	}
	return output
}
